﻿using UnityEngine;

public class ShootRpc : MonoBehaviour
{
    public GameObject prefab;
    [PunRPC]
    void Shoot(Vector3 position, Quaternion rotation, Vector3 velocity, Vector3 angularVelocity)
    {
        Debug.Log("get Shoot");
        var go = GameObject.Instantiate(prefab);
        var rigidbody = go.GetComponent<Rigidbody>();
        go.transform.position = position;
        go.transform.rotation = rotation;
        rigidbody.velocity = velocity;
        rigidbody.angularVelocity = angularVelocity;
        rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
        Object.Destroy(go, 15.0f);
    }
}