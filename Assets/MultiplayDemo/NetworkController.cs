﻿using UnityEngine;
using System.Collections;
using Photon;

public class NetworkController : Photon.PunBehaviour {

    private PhotonView myPhontonView = null;
	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings ("0.1");
		// PhotonNetwork.logLevel = PhotonLogLevel.Full;
	}

	void OnGUI(){
		GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());
	}

	public override void OnJoinedLobby ()
	{
		PhotonNetwork.JoinRandomRoom ();
	}

	public override void OnPhotonRandomJoinFailed (object[] codeAndMsg)
	{
		Debug.Log ("Can't join random room!");
		foreach (object msg in codeAndMsg) {
			Debug.Log (msg.ToString ());
		}
		PhotonNetwork.CreateRoom (null);
	}

	public override void OnJoinedRoom ()
	{
		GameObject hmd = PhotonNetwork.Instantiate ("HMDObject", Vector3.zero, Quaternion.identity, 0);
        hmd.GetComponent<HmdMovementController>().isMine = true;
        this.myPhontonView = hmd.GetPhotonView();
    }

    public PhotonView GetPhotonView()
    {
        return this.myPhontonView;
    }

}
