﻿using UnityEngine;
using System.Collections;

public class HmdMovementController : MonoBehaviour
{
    private GameObject hmd;
    public bool isMine = false;
    void Start()
    {
        hmd = GameObject.Find("VrRoom/Camera (head)");
    }
    // Update is called once per frame
    void Update()
    {
        if (this.isMine)
        {
            Transform transform = this.GetComponent<Transform>();
            transform.rotation = hmd.transform.rotation;
            transform.position = hmd.transform.position;
        }

    }
}
