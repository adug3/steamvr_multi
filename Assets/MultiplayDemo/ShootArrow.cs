﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class ShootArrow : MonoBehaviour
{
    public GameObject prefab;
    public Rigidbody attachPoint;
    private PhotonView myPhotonView = null;

    SteamVR_TrackedObject trackedObj;
    FixedJoint joint;

    private GameObject hmd;

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        Debug.Log("network controller: " + GameObject.Find("NetworkObject").GetComponent<NetworkController>());
        myPhotonView = GameObject.Find("NetworkObject").GetComponent<NetworkController>().GetPhotonView();
    }

    void FixedUpdate()
    {
        var device = SteamVR_Controller.Input((int)trackedObj.index);
        if (joint == null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            var go = GameObject.Instantiate(prefab);
            go.transform.position = attachPoint.transform.position;
            go.transform.rotation = attachPoint.transform.rotation;

            joint = go.AddComponent<FixedJoint>();
            joint.connectedBody = attachPoint;
        }
        else if (joint != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            var go = joint.gameObject;
            var origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;

            Vector3 velocity = (device.transform.rot * Vector3.forward) * 5;
            velocity += origin.TransformVector(device.velocity);
            Vector3 angularVelocity = origin.TransformVector(device.angularVelocity);
            Vector3 position = go.transform.position;
            Quaternion rotation = go.transform.rotation;

            Object.DestroyImmediate(joint.gameObject);
            Object.DestroyImmediate(joint);
            
            if (myPhotonView == null)
            {
                myPhotonView = GameObject.Find("NetworkObject").GetComponent<NetworkController>().GetPhotonView();
            }
            if (myPhotonView != null)
            {
                Debug.Log("Send RPC shoot");
                myPhotonView.RPC("Shoot", PhotonTargets.All, position, rotation, velocity, angularVelocity);
            }else
            {
                Debug.LogError("PhotonView is null!");
            }



            /*var go = joint.gameObject;
            var rigidbody = go.GetComponent<Rigidbody>();
            Object.DestroyImmediate(joint);
            joint = null;
            Object.Destroy(go, 15.0f);

            var origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
            if (origin != null)
            {
                Vector3 pointTo = device.transform.rot * Vector3.forward;
                rigidbody.velocity = pointTo * 5;
                rigidbody.velocity += origin.TransformVector(device.velocity);
                rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
            }
            else
            {
                rigidbody.velocity = device.velocity;
                rigidbody.angularVelocity = device.angularVelocity;
            }

            rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;*/
        }
    }
}
